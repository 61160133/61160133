import java.util.Scanner;
public class OX {
    public static Scanner kb = new Scanner(System.in);
    public static boolean checkInput = false;
    public static char turn = 'O';
    public static String checkBreak = "notBreak", checkNumber = "notNumber";
    public static String checkWin = "noWin";
    public static int round = 0 ,row = 0 , col = 0;
    public static char table [][] = {{'_','_','_'},
                                    {'_','_','_'},
                                    {'_','_','_'}};
    public static void welcome(){
       System.out.println("Welcome to OX Game"); 
    }
    public static void showTable(){ 
        for(int i = 0; i < 3; i++ ) {
            for(int j = 0; j < 3; j++) {
                System.out.print(table[i][j]);
                System.out.print(" ");	
            }
                System.out.println();
        }
    }
    public static void showTurn(){ 
        System.out.println("Turn : "+turn);
    }
    public static void switchTurn(){
        if(turn == 'O'){
            turn = 'X';
        }else{
            turn = 'O';
        }
    }
    public static void showInput(){
        System.out.print("please input row and column : ");
    }
    public static void input(){
        while (round < 5 && checkBreak == "notBreak"){
            checkInput = false;
            checkInputO();
            switchTurn();
            if(round < 4 && checkBreak == "notBreak") {
            	checkInputX();
            }
            round++;
        } 
    }
    public static void getInput() {
    	row = kb.nextInt();
        col = kb.nextInt();
    }
    public static void checkInputX(){
        checkInput = false;
                while(checkInput == false) {
                    showTurn();showInput();
                    getInput();
                    if(row>=1 && row<=3  && col>=1 && col<=3 && table[row-1][col-1] == '_'){
                        table[row-1][col-1] = 'X';
                        checkInput = true;
                    }else {
                    	showWrong();
                    }showTable();  
                    if(checkWinO() == "win" || checkWinX() == "win" ){
                        checkBreak = "break";
                    }
                }switchTurn();
    }
    public static void checkInputO() {
    	while(checkInput == false){
            showTurn();showInput();
            getInput();
            if(row>=1 && row<=3 && col>=1 && col<=3 && table[row-1][col-1] == '_'){ 
                table[row-1][col-1] = 'O';
                checkInput = true;
            }else {
            	showWrong();
            }
            showTable(); 
            if(checkWinO() == "win" || checkWinX() == "win"){
                checkBreak = "break";
            }
        }
    }	
    public static String checkWinO(){
        if(table[0][0] == 'O' && table[1][0] == 'O' && table[2][0] == 'O' ||
           table[0][1] == 'O' && table[1][1] == 'O' && table[2][1] == 'O' ||
           table[0][2] == 'O' && table[1][2] == 'O' && table[2][2] == 'O' ||
           table[0][0] == 'O' && table[0][1] == 'O' && table[0][2] == 'O' ||
           table[1][0] == 'O' && table[1][1] == 'O' && table[1][2] == 'O' ||
           table[2][0] == 'O' && table[2][1] == 'O' && table[2][2] == 'O' ||
           table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O' ||   
           table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O' ) {
        	showWinO();
        	showBye();
        	checkWin = "someOneWin"; return "win";
        }
        else {
            return "error";
        }   
    }
    public static String checkWinX(){
        if(table[0][0] == 'X' && table[1][0] == 'X' && table[2][0] == 'X' ||
           table[0][1] == 'X' && table[1][1] == 'X' && table[2][1] == 'X' ||
           table[0][2] == 'X' && table[1][2] == 'X' && table[2][2] == 'X' ||
           table[0][0] == 'X' && table[0][1] == 'X' && table[0][2] == 'X' ||
           table[1][0] == 'X' && table[1][1] == 'X' && table[1][2] == 'X' ||
           table[2][0] == 'X' && table[2][1] == 'X' && table[2][2] == 'X' ||
           table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X' ||   
           table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X' ) {
        	showWinX();
        	showBye();
        	checkWin = "someOneWin"; return "win";
        }
        else {
            return "error";
        }   
    }
    public static void checkDraw(){
        if(checkWin == "noWin") {
            showDraw();
            showBye();
        }
    }
    public static void showWrong() {
    	System.out.println("please input row and column again");
    }
    public static void showWinX() {
    	System.out.println("+---- X WIN ----+");
    }
    public static void showWinO() {
    	System.out.println("+---- O WIN ----+");
    }
    public static void showDraw() {
    	System.out.println("+----DRAW----+");
    }
    public static void showBye() {
    	System.out.println("Bye Bye .");
    }
    //public static char checkText(int row,int col) {
     // for(char text='A'; text<='Z'; text++){
    //	 if(text )
      //}
      
    //}
    public static void main(String args[]) {
        welcome();
        showTable();
        input();
        checkDraw();
    } 
}
